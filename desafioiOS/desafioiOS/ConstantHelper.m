//
//  ConstantHelper.m
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "ConstantHelper.h"

@implementation ConstantHelper

NSString *const kUserRepositoryCellIdentifier = @"userRepositoryCell";
NSString *const kUserPullRequestCellIdentifier = @"userPullRequestCell";

NSString *const kRestServiceBaseURL = @"https://api.github.com/";
NSString *const kRestServiceRepositoryList = @"search/repositories";
NSString *const kRestServicePullRequestList = @"repos/:owner/:repository/pulls";

NSString *const kSegueRepositoryToPullRequest = @"RepositoryToPullRequest";
NSString *const kSeguePullRequestToWebDetail = @"PullRequestToWebDetail";

@end
