//
//  MappingHelper.h
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface MappingHelper : NSObject

+(RKObjectMapping*)repositoryMapping;
+(RKObjectMapping*)pullRequestMapping;
+(RKObjectMapping*)ownerMapping;
+(RKObjectMapping*)repositoryListMapping;

@end
