//
//  RepositoryList.h
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepositoryList : NSObject

@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSArray  *items;

@end
