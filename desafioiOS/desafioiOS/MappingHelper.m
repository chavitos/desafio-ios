//
//  MappingHelper.m
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "MappingHelper.h"
#import "Repository.h"
#import "PullRequest.h"
#import "Owner.h"
#import "RepositoryList.h"

@implementation MappingHelper

+(RKObjectMapping*)repositoryMapping{
    
    RKObjectMapping *repositoryMapping = [RKObjectMapping mappingForClass:[Repository class]];
    [repositoryMapping addAttributeMappingsFromDictionary:@{ @"name": @"repositoryName"
                                                            ,@"full_name": @"fullName"
                                                            ,@"description": @"repositoryDescription"
                                                            ,@"forks_count": @"forks"
                                                            ,@"stargazers_count": @"stargazers"}];
    
    [repositoryMapping addRelationshipMappingWithSourceKeyPath:@"owner" mapping:[MappingHelper ownerMapping]];
    
    return repositoryMapping;
}

+(RKObjectMapping*)pullRequestMapping{
    
    RKObjectMapping *pullRequestMapping = [RKObjectMapping mappingForClass:[PullRequest class]];
    [pullRequestMapping addAttributeMappingsFromArray:@[ @"title"
                                                        ,@"body"
                                                        ,@"state"]];
    
    [pullRequestMapping addAttributeMappingsFromDictionary:@{@"html_url":@"htmlUrl"}];
    
    [pullRequestMapping addRelationshipMappingWithSourceKeyPath:@"user" mapping:[MappingHelper ownerMapping]];
    
    return pullRequestMapping;
}

+(RKObjectMapping*)ownerMapping{
    
    RKObjectMapping *ownerMapping = [RKObjectMapping mappingForClass:[Owner class]];
    [ownerMapping addAttributeMappingsFromDictionary:@{@"avatar_url":@"urlAvatar"}];
    [ownerMapping addAttributeMappingsFromArray:@[ @"login"]];
    
    return ownerMapping;
}

+(RKObjectMapping*)repositoryListMapping{
    
    RKObjectMapping *repositoryListMapping = [RKObjectMapping mappingForClass:[RepositoryList class]];
    [repositoryListMapping addAttributeMappingsFromDictionary:@{@"total_count": @"totalCount"}];
    
    [repositoryListMapping addRelationshipMappingWithSourceKeyPath:@"items" mapping:[MappingHelper repositoryMapping]];
    
    return repositoryListMapping;
}

@end
