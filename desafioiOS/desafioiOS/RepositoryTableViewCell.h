//
//  RepositoryTableViewCell.h
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Repository.h"

@interface RepositoryTableViewCell : UITableViewCell

-(void)configCellWithRepository:(Repository*)repository;

@end
