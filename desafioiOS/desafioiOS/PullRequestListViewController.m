//
//  PullRequestListViewController.m
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "PullRequestListViewController.h"
#import "PullRequest+Search.h"
#import "PullRequestTableViewCell.h"
#import "ConstantHelper.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "PullRequestWebDetailViewController.h"

@interface PullRequestListViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UILabel *openedLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedLabel;

@property (strong,nonatomic) NSArray *pullRequestArray;

@property (nonatomic) NSInteger openedPullRequest;
@property (nonatomic) NSInteger closedPullRequest;

@property (strong,nonatomic) PullRequest *selectedPullRequest;

@end

@implementation PullRequestListViewController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configComponents];
    [self getPullRequests];
}

#pragma mark - Private Methods

-(void)configComponents{
    
    self.openedPullRequest = 0;
    self.closedPullRequest = 0;
    
    self.title = self.repository.repositoryName;
}

-(void)getPullRequests{
    
    [SVProgressHUD show];
    [PullRequest.new getPullRequestListFromRepository:self.repository WithHandler:^(NSArray *pullRequestList, NSError *error) {
        
        if (!error) {
            
            if (pullRequestList.count > 0) {
                
                self.pullRequestArray = pullRequestList;
                
                //for para fazer o count dos status dos poull requests (open e close) que serão exibidos no header
                for (PullRequest *pullRequest in pullRequestList) {
                    
                    if ([pullRequest.state isEqualToString:@"open"]) {
                        
                        self.openedPullRequest++;
                    }else{
                        
                        self.closedPullRequest++;
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.tableview reloadData];
                });
            }
        }
        
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - IBAction

- (IBAction)back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.pullRequestArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PullRequestTableViewCell *cell = [self.tableview dequeueReusableCellWithIdentifier:kUserPullRequestCellIdentifier];
    
    [cell configCellWithRepository:self.pullRequestArray[indexPath.row]fromRepository:self.repository.repositoryName];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [[self.tableview cellForRowAtIndexPath:indexPath] setSelected:NO];
    
    self.selectedPullRequest = self.pullRequestArray[indexPath.row];
    [self performSegueWithIdentifier:kSeguePullRequestToWebDetail sender:self];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    [self.openedLabel setText:[NSString stringWithFormat:@"%d opened ",self.openedPullRequest]];
    [self.closedLabel setText:[NSString stringWithFormat:@"/ %d closed",self.closedPullRequest]];
    
    return [self headerView];
}

#pragma mark - Segues methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:kSeguePullRequestToWebDetail]) {
        
        PullRequestWebDetailViewController *pullRequestWebDetailViewController = segue.destinationViewController;
        [pullRequestWebDetailViewController setPullRequest:self.selectedPullRequest];
    }
}

@end
