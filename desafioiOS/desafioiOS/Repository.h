//
//  Repository.h
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Owner.h"

@interface Repository : NSObject

@property (strong,nonatomic) NSString *repositoryName;
@property (strong,nonatomic) NSString *fullName;
@property (strong,nonatomic) NSString *repositoryDescription;
@property (strong,nonatomic) NSNumber *forks;
@property (strong,nonatomic) NSNumber *stargazers;
@property (strong,nonatomic) Owner *owner;

@end
