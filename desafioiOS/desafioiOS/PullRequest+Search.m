//
//  PullRequest+Search.m
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "PullRequest+Search.h"

@implementation PullRequest (Search)

-(void)getPullRequestListFromRepository:(Repository*)repository WithHandler:(pullRequestListBlock)handler{
    
    [PullRequestManager.new getPullRequestListFromRepository:repository WithHandler:handler];
}

@end
