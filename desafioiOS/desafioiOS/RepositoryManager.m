//
//  RepositoryManager.m
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "RepositoryManager.h"
#import <RestKit/RestKit.h>
#import "MappingHelper.h"
#import "ConstantHelper.h"

@implementation RepositoryManager

-(void)getRepositoryListWithParams:(NSDictionary*)params AndHandler:(repositoryListBlock)handler{
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:kRestServiceBaseURL]];
    [manager addResponseDescriptor:[self setupResponseDescriptors]];
    
    [manager getObjectsAtPath:kRestServiceRepositoryList parameters:params success:^(RKObjectRequestOperation *operation,
                                                                                                  RKMappingResult *mappingResult) {
        
        NSLog(@"Busca de repositórios concluída com sucesso!");
        handler(mappingResult.array.firstObject,nil);
        
    } failure:^(RKObjectRequestOperation *operation,
                NSError *error) {
        
        NSLog(@"Erro ao buscar repositórios >>> %@",error.description);
        handler(nil,error);
    }];
}

-(RKResponseDescriptor*)setupResponseDescriptors {

    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[MappingHelper repositoryListMapping] method:RKRequestMethodGET pathPattern:kRestServiceRepositoryList keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    return responseDescriptor;
}

@end
