//
//  PullRequestTableViewCell.m
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "PullRequestTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PullRequestTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;

@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;

@end

@implementation PullRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Config Method

-(void)configCellWithRepository:(PullRequest*)pullRequest fromRepository:(NSString*)repositoryName{

    //corner radius da largura (ou altura) do quadrado /2 para virar um circulo
    [self.userImage.layer setCornerRadius:CGRectGetWidth(self.userImage.frame)/2];  
    
    [self.titleLabel setText:pullRequest.title];
    [self.bodyLabel setText:pullRequest.body];
    [self.userNameLabel setText:pullRequest.user.login];
    [self.fullNameLabel setText:[NSString stringWithFormat:@"%@/%@",pullRequest.user.login,repositoryName]];
    
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:pullRequest.user.urlAvatar]
                 placeholderImage:[UIImage imageNamed:@"user-image"]];
}

@end
