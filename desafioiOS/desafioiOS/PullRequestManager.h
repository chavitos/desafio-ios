//
//  PullRequestManager.h
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PullRequest.h"
#import "Repository.h"

typedef void(^pullRequestListBlock)(NSArray *pullRequestList, NSError *error);

@interface PullRequestManager : NSObject

-(void)getPullRequestListFromRepository:(Repository*)repository WithHandler:(pullRequestListBlock)handler;

@end
