//
//  PullRequestTableViewCell.h
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequest.h"

@interface PullRequestTableViewCell : UITableViewCell

-(void)configCellWithRepository:(PullRequest*)pullRequest fromRepository:(NSString*)repositoryName;

@end
