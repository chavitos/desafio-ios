//
//  ConstantHelper.h
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConstantHelper : NSObject

extern NSString *const kUserRepositoryCellIdentifier;
extern NSString *const kUserPullRequestCellIdentifier;

extern NSString *const kRestServiceBaseURL;
extern NSString *const kRestServiceRepositoryList;
extern NSString *const kRestServicePullRequestList;

extern NSString *const kSegueRepositoryToPullRequest;
extern NSString *const kSeguePullRequestToWebDetail;

@end
