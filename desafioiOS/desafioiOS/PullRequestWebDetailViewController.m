//
//  PullRequestWebDetailViewController.m
//  desafioiOS
//
//  Created by Tiago Chaves on 12/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "PullRequestWebDetailViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface PullRequestWebDetailViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation PullRequestWebDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.pullRequest.title;
    
    [SVProgressHUD show];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.pullRequest.htmlUrl]]];
}

#pragma mark - IBAction

- (IBAction)back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebViewDelegate

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [SVProgressHUD dismiss];
}

@end
