//
//  RepositoryManager.h
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositoryList.h"

typedef void(^repositoryListBlock)(RepositoryList *repositoryList, NSError *error);

@interface RepositoryManager : NSObject

-(void)getRepositoryListWithParams:(NSDictionary*)params AndHandler:(repositoryListBlock)handler;

@end
