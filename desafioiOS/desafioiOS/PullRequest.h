//
//  PullRequest.h
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Owner.h"

@interface PullRequest : NSObject

@property (strong,nonatomic) NSString *title;
@property (strong,nonatomic) NSString *body;
@property (strong,nonatomic) NSString *state;
@property (strong,nonatomic) NSString *htmlUrl;

@property (strong,nonatomic) Owner *user;

@end
