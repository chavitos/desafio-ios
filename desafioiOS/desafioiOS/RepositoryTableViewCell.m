//
//  RepositoryTableViewCell.m
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "RepositoryTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RepositoryTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *repositoryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *repositoryDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *numberOfForksLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfStargazersLabel;

@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;

@end

@implementation RepositoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Config Method

-(void)configCellWithRepository:(Repository*)repository{
    
    [self.repositoryNameLabel setText:repository.repositoryName];
    [self.repositoryDescriptionLabel setText:repository.repositoryDescription];
    
    [self.numberOfForksLabel setText:repository.forks.stringValue];
    [self.numberOfStargazersLabel setText:repository.stargazers.stringValue];
    
    [self.userNameLabel setText:repository.owner.login];
    [self.fullNameLabel setText:repository.fullName];
    
    //corner radius da largura (ou altura) do quadrado /2 para virar um circulo
    [self.userImage.layer setCornerRadius:CGRectGetWidth(self.userImage.frame)/2];
    
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:repository.owner.urlAvatar]
                      placeholderImage:[UIImage imageNamed:@"user-image"]];
}

@end
