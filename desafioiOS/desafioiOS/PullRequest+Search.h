//
//  PullRequest+Search.h
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "PullRequest.h"
#import "PullRequestManager.h"

@interface PullRequest (Search)

-(void)getPullRequestListFromRepository:(Repository*)repository WithHandler:(pullRequestListBlock)handler;

@end
