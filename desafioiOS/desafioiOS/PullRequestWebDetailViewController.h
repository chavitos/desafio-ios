//
//  PullRequestWebDetailViewController.h
//  desafioiOS
//
//  Created by Tiago Chaves on 12/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequest.h"

@interface PullRequestWebDetailViewController : UIViewController

@property (strong,nonatomic) PullRequest *pullRequest;

@end
