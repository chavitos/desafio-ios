//
//  RepositoryList+Search.m
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "RepositoryList+Search.h"

@implementation RepositoryList (Search)

-(void)getRepositoryListWithParams:(NSDictionary*)params AndHandler:(repositoryListBlock)handler{

    [RepositoryManager.new getRepositoryListWithParams:params AndHandler:handler];
}

@end
