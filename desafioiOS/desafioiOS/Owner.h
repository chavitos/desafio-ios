//
//  Owner.h
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Owner : NSObject

@property (strong,nonatomic) NSString *login;
@property (strong,nonatomic) NSString *urlAvatar;

@end
