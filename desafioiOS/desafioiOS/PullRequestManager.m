//
//  PullRequestManager.m
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "PullRequestManager.h"
#import <RestKit/RestKit.h>
#import "MappingHelper.h"
#import "ConstantHelper.h"

@implementation PullRequestManager

-(void)getPullRequestListFromRepository:(Repository*)repository WithHandler:(pullRequestListBlock)handler{
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:kRestServiceBaseURL]];
    [manager addResponseDescriptor:[self setupResponseDescriptors]];
    
    //Objeto contendo as informações necessárias para preencher o path.
    //Feito dessa forma por a informação login está dentro de outro objeto (Owner, dentro do objecto repositório)
    NSDictionary *object = @{@"owner":repository.owner.login
                            ,@"repository":repository.repositoryName};
    
    [manager getObjectsAtPath:RKPathFromPatternWithObject(kRestServicePullRequestList,object) parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        NSLog(@"Busca de pull requests concluída com sucesso!");
        handler(mappingResult.array,nil);
        
    } failure:^(RKObjectRequestOperation *operation,
                NSError *error) {
        
        NSLog(@"Erro ao buscar pull requests >>> %@", error.description);
        handler(nil,error);
    }];
}

-(RKResponseDescriptor*)setupResponseDescriptors {
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[MappingHelper pullRequestMapping] method:RKRequestMethodGET pathPattern:kRestServicePullRequestList keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    return responseDescriptor;
}

@end
