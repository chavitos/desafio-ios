//
//  RepositoryListViewController.m
//  desafioiOS
//
//  Created by Tiago Chaves on 10/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "RepositoryListViewController.h"
#import "PullRequestListViewController.h"
#import "RepositoryList+Search.h"
#import "RepositoryTableViewCell.h"
#import "ConstantHelper.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface RepositoryListViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) NSNumber *page; //Variável para controle da paginação
@property (strong, nonatomic) Repository *selectedRepository;
@property (strong, nonatomic) NSMutableArray *repositoriesArray;

@property (nonatomic) BOOL canLoadMoreData; //boleano para controlar o load de novos repositórios através do scroll
@property (nonatomic) NSInteger numberOfRepositories; //número de repositórios já retornados

@end

@implementation RepositoryListViewController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configComponents];
    [self getRepositoryList];
}

#pragma mark - Private Methods

-(void)configComponents{
    
    //setar os valores iniciais das variáveis
    self.page = @1;
    self.repositoriesArray = [NSMutableArray new];
    self.canLoadMoreData = YES;
    self.numberOfRepositories = 0;
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.title = @"Github Java Pop";
}

-(void)getRepositoryList{
    
    //?q=language:Java&sort=stars&page=
    //Query params do serviço
    NSDictionary *params = @{@"q":@"language:Java"
                            ,@"sort":@"stars"
                            ,@"page":self.page};
    
    [SVProgressHUD show];
    
    [RepositoryList.new getRepositoryListWithParams:params AndHandler:^(RepositoryList *repositoryList, NSError *error) {
        
        if (!error) {
            
            if (repositoryList.items.count > 0) {
                
                
                [self.repositoriesArray addObjectsFromArray:repositoryList.items];
                self.numberOfRepositories += self.repositoriesArray.count;
                
                //Se não baixou todos os repositórios ainda...
                if (repositoryList.totalCount.integerValue > self.numberOfRepositories) {
                    
                    //incrementar o controle de paginação para buscar os próximos repositórios
                    self.page = [NSNumber numberWithInteger:self.page.integerValue + 1];
                    //deixa a chamada do método "ativa" pelo scroll até o fim
                    self.canLoadMoreData = YES;
                }else{
                    
                    //se já tem todos os repositórios, não deixa mais chamar o método de busca
                    self.canLoadMoreData = NO;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.tableview reloadData];
                });
            }
        }
        
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - UITableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.repositoriesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RepositoryTableViewCell *cell = [self.tableview dequeueReusableCellWithIdentifier:kUserRepositoryCellIdentifier];
    [cell configCellWithRepository:self.repositoriesArray[indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //"salva" o repositório selecionado para enviar os dados para a tela de pulls do mesmo
    self.selectedRepository = self.repositoriesArray[indexPath.row];
    
    [self performSegueWithIdentifier:kSegueRepositoryToPullRequest sender:self];
    [[self.tableview cellForRowAtIndexPath:indexPath] setSelected:NO];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat actualPosition = self.tableview.contentOffset.y;
    
    //valor de referencia para fazer o refresh de dados deve ser o tamanho da tabela (topo) + meia célula
    CGFloat contentHeight = self.tableview.contentSize.height - CGRectGetHeight(self.tableview.frame) + (self.tableview.rowHeight/2);
    
    //se passar o limite do scroll da tabela...
    if (actualPosition >= contentHeight) {
        
        //se ainda é possível buscar repositórios
        if (self.canLoadMoreData) {
            
            //seta variável de conrole para NO assim só chama o méotodo quando a primeira chamada tiver sucesso
            self.canLoadMoreData = NO;
            //busca mais repositórios
            [self getRepositoryList];
        }
    }
}

#pragma mark - Segues methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:kSegueRepositoryToPullRequest]) {
        
        PullRequestListViewController *pullRequestListViewController = segue.destinationViewController;
        [pullRequestListViewController setRepository:self.selectedRepository];
    }
}

@end
