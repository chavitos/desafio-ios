//
//  RepositoryList+Search.h
//  desafioiOS
//
//  Created by Tiago Chaves on 11/03/17.
//  Copyright © 2017 concreteSolutions. All rights reserved.
//

#import "RepositoryList.h"
#import "RepositoryManager.h"

@interface RepositoryList (Search)

-(void)getRepositoryListWithParams:(NSDictionary*)params AndHandler:(repositoryListBlock)handler;

@end
